import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { accountEntity } from '../account/account.entity';
import { transactionEntity } from '../transaction/transaction.entity';

@Entity('user')
export class userEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  pin: number;
  @OneToMany(() => accountEntity, (account) => account.user)
  accounts: accountEntity[];
  @OneToMany(() => transactionEntity, (transaction) => transaction.user)
  transactions: accountEntity[];
}
