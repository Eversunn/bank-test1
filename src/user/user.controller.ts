import { Controller, Delete, Get, Param, Post } from '@nestjs/common';

import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get()
  findAll();

  @Get(':id')
  findOne(@Param('id') id: string) {}
  @Post()
  create();
  @Delete(':id')
  remove();
}
