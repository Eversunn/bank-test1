import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { userEntity } from './user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(userEntity)
    private userRepository: Repository<userEntity>,
  ) {}
  findAll(): Promise<userEntity[]> {
    return this.userRepository.find();
  }

  findOne(id: string): Promise<userEntity> {
    return this.userRepository.findOneBy({ id });
  }
  async remove(id: string): Promise<void> {
    await this.userRepository.delete(id);
  }
}
