import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { userEntity } from '../user/user.entity';
import { transactionEntity } from '../transaction/transaction.entity';

@Entity('account')
export class accountEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  balance: number;

  @ManyToOne(() => userEntity, (user) => user.accounts)
  user: userEntity;
  @OneToMany(() => transactionEntity, (transaction) => transaction.account)
  transactions: accountEntity[];
}
