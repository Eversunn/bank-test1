import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { accountEntity } from '../account/account.entity';
import { userEntity } from 'src/user/user.entity';

@Entity('transactions')
export class transactionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  value: number;
  @ManyToOne(() => userEntity, (user) => user.transactions)
  user: userEntity;

  @ManyToOne(() => accountEntity, (account) => account.transactions)
  account: accountEntity;

  @Column()
  Type: string;
}
